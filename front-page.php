<?php get_header("large"); ?>

	<div class="uk-container uk-container-center home-page">
	<?php while (have_posts()) : the_post(); ?>
		<article class="uk-article">
			<?php the_content(); ?>
		</article>
	<?php endwhile; ?>
	</div>

<?php get_footer(); ?>