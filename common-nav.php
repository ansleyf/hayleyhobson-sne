<?php
    $menuargs = array(
        "container"         => "",
        "theme_location"    => "primary_nav",
        "menu_class"        => "uk-navbar-nav uk-visible-large",
        "menu_id"           => "",
        'walker'            => new Walker_UIKIT
    );
    $logged_inargs = array(
        "container"         => "",
        "theme_location"    => "logged_in",
        "menu_class"        => "uk-navbar-nav uk-visible-large",
        "menu_id"           => "",
        'walker'            => new Walker_UIKIT
    );
    $offcanv = array(
        "container"         => "",
        "theme_location"    => "primary_nav",
        "menu_class"        => "uk-nav uk-nav-offcanvas",
        "menu_id"           => ""
	);
	$u_offcan = array(
        "container"         => "",
        "theme_location"    => "logged_in",
        "menu_class"        => "uk-nav uk-nav-offcanvas",
        "menu_id"           => ""
	);
?>
<nav class="uk-navbar main">
	<a href="#offcanv" class="uk-navbar-toggle uk-hidden-large" data-uk-offcanvas>Site Menu </a>
	<?php if(is_user_logged_in()) : ?>
		<a href="#u-offcanv" class="uk-navbar-toggle uk-float-right uk-hidden-large" data-uk-offcanvas>User Menu </a>
	<?php endif; ?>
	<div class="uk-container uk-container-center"><?php wp_nav_menu($menuargs); ?></div>
</nav>
<?php if(is_user_logged_in()) : ?>
	<nav class="uk-navbar logged-in uk-visible-large">
		<div class="uk-container uk-container-center"><?php wp_nav_menu($logged_inargs); ?></div>
	</nav>
<?php endif; ?>
<div id="offcanv" class="uk-offcanvas">
	<div class="uk-offcanvas-bar uk-offcanvas-bar">
		<?php wp_nav_menu($offcanv); ?>
	</div>
</div>
<div id="u-offcanv" class="uk-offcanvas">
	<div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
		<?php wp_nav_menu($u_offcan); ?>
	</div>
</div>