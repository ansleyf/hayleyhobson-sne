<?php
/**
    footer.php - Version 0.1.0
*/
?>
		<footer class="uk-width-1-1 uk-margin-large-top">
			<div class="ak">
				<div class="uk-text-center uk-text-small">
					&copy; 2014 <?php echo get_bloginfo( "name", "raw"); ?>. All Rights Reserved. &bull; <a href="http://ansleyfones.com/">Design &amp; Development by Ansley Fones</a>
				</div>
			</div>
		</footer>
	<?php wp_footer(); ?>
	</body>
</html>